## Запуск проекта
####Первый
1. make build OR docker-compose up --build

####Последующие
1. make up OR docker-compose up -d (при каждом перезапуске база пересоздается заново для упрощения развертывания)

---

## API
###Регистрация и авторизация
####POST /api/register
####POST /api/login
#####Запрос
| Параметры | Тип | Описание |
| ------ | ------ | ------ |
| email | string | мыло |
| password | string | латиница, цифры, тире, подчеркивание, не менее 6 символов |
#####Ответ
```
{
    "token":"eyJ0eXAiOiJK.........TKFfnBuk"
}
```
Используем полученный токен в заголовке "Authorization: Bearer $token" для остальных запросов

---
###Продукты
####GET /api/v1/products
получить инфу о всех продуктах
####GET /api/v1/products/{id}
получить инфу о продукте {id}

---
###Заказы
####GET /api/v1/orders
получить инфу о всех заказах
####GET /api/v1/orders/{id}
получить инфу о заказе {id} + возможные статусы для смены
####POST /api/v1/orders
создать новый заказ
####PUT /api/v1/orders/{id}
сменить статус заказа (параметр 'status_id') согласно схеме
####DELETE /api/v1/orders/{id}
удалить заказ {id}

---
###Продукты заказа
####GET /api/v1/orders/{order_id}/products
получить инфу о продуктах и их количестве в заказе {order_id}
####GET /api/v1/orders/{order_id}/products/{product_id}
получить инфу о продукте {product_id} в заказе {order_id}
####POST /api/v1/orders/{order_id}/products
добавить продукт в заказ (обязательный парметр product_id). при повторном запросе +1 к кол-ву данного товара в заказе
####PUT /api/v1/orders/{order_id}/products/{product_id}
изменить продукт {product_id} в заказе {order_id} (необязательный параметр 'quantity')
####DELETE /api/v1/orders/{id}
удалить продукт {product_id} из заказа {order_id}
