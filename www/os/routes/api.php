<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'PassportController@register')->middleware('throttle:5,5');
Route::post('login', 'PassportController@login')->middleware('throttle:5,2');
Route::post('logout', 'PassportController@logout');

Route::middleware('auth:api')->namespace('Api')->group(function (){
//    Route::model('orders', 'Order');
//    Route::model('products', 'Product');
    Route::namespace('V1')->prefix('v1')->group(function (){
        Route::get('user', 'PassportController@details');
        Route::apiResource('products', 'ProductController')->only(['index','show']);
        Route::apiResource('orders', 'OrderController');
        Route::apiResource('orders/{order}/products', 'OrderProductsController');
    });
    
});

