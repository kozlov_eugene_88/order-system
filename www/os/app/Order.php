<?php

namespace App;

use App\Scopes\MainScope;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $model = null;
    
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new MainScope());
    }
    
    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function products() {
        return $this->hasMany('App\OrderProduct');
    }
    
    public function status() {
        return $this->belongsTo('App\OrderStatus');
    }
}
