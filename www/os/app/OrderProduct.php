<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $guarded = [];
    protected $casts = [
        'quantity' => 'integer',
    ];
    
    public function product() {
        return $this->hasOne('App\Product', 'id','product_id');
    }
    
    public function order() {
        return $this->belongsTo('App\Order');
    }
}
