<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'               => 'Order',
            'id'                 => $this->id,
            'status'             => [
                'id'   => $this->status_id,
                'name' => $this->status->name,
            ],
            'allowed_statuses_id' => $this->status->allowed->pluck('allowed_status_id'),
        ];
    }
}
