<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\ProductResource;
use App\Http\Resources\V1\ProductsResource;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return new ProductsResource(Product::paginate());
    }
    
    public function show(Product $product)
    {
        return new ProductResource($product);
    }
}
