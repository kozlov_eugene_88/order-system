<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\OrdersRequest;
use App\Http\Resources\V1\OrderResource;
use App\Http\Resources\V1\OrdersResource;
use App\Order;
use App\Services\Orders\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(OrderService $orderService)
    {
        if (!$response = $orderService->get()){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrdersResource($response);
    }
    
    public function store(OrdersRequest $request, OrderService $orderService)
    {
        if (!$response = $orderService->create()){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderResource($response);
    }
    
    public function show(OrderService $orderService, $order)
    {
        if (!$response = $orderService->show($order)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderResource($response);
    }
    
    public function update(OrdersRequest $request, OrderService $orderService, $order)
    {
        if(!$response = $orderService->update($order, $request->status_id)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderResource($response);
    }
    
    public function destroy(OrderService $orderService, $order)
    {
        if (!$response = $orderService->destroy($order)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return response()->json(self::RESPONSE_OK);
    }
}
