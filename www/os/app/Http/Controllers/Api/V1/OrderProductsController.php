<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\OrderProductsRequest;
use App\Http\Resources\V1\OrderProductResource;
use App\Http\Resources\V1\OrderProductsResource;
use App\Order;
use App\OrderProduct;
use App\Http\Controllers\Controller;
use App\Product;
use App\Services\Orders\OrderProductService;
use Illuminate\Http\Request;

class OrderProductsController extends Controller
{
    public function index(OrderProductsRequest $request, OrderProductService $ops, $order)
    {
        if (!$response = $ops->get($order)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderProductsResource($response);
    }
    
    public function store(OrderProductsRequest $request, OrderProductService $ops, $order)
    {
        if (!$response = $ops->create($order, $request->product_id)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderProductResource($response);
    }
    
    public function show(OrderProductService $ops, $order, $product)
    {
        if (!$response = $ops->show($order, $product)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderProductResource($response);
    }
    
    public function update(OrderProductsRequest $request, OrderProductService $ops, $order, $product)
    {
        if (!$response = $ops->update($order, $product, $request->quantity)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return new OrderProductResource($response);
    }
    
    public function destroy(OrderProductService $ops, $order, $product)
    {
        if (!$response = $ops->destroy($order, $product)){
            return response()->json(self::RESPONSE_FAIL, 400);
        }
        return response()->json(self::RESPONSE_OK);
    }
}
