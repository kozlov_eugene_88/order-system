<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiLoginRequest;
use App\Http\Requests\ApiRegisterRequest;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class PassportController extends Controller
{
    use AuthenticatesUsers;
    
    public function register(ApiRegisterRequest $request)
    {
        $user = User::create([
            'name'     => $request->email,
            'email'    => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $token = $user->createToken('Passport Personal Token')->accessToken;
        return response()->json(['token' => $token], 200);
    }
    
    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->attemptLogin($request)) {
            $user  = $this->guard()->user();
            $token = $user->createToken('TutsForWeb')->accessToken;
            return response()->json(['token' => $token], 200);
        }else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }
    
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['data' => 'User logged out.'], 200);
    }
    
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
