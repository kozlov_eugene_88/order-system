<?php

namespace App\Services\Orders;

use App\Order;
use App\OrderProduct;
use Throwable;

class OrderProductService
{
    public function get($order_id, $all = false)
    {
        try{
            $order    = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->with('products')->findOrFail($order_id);
            $products = $order->products()->paginate();
        }catch (Throwable $e){
            return false;
        }
        return $products;
    }
    
    public function create($order_id, $product_id, $all = false)
    {
        try{
            $order_product = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->with('products')->findOrFail($order_id)->products()->where('product_id', $product_id)->first();
            if ($order_product) {
                $order_product->increment('quantity');
                return $order_product;
            }
            $order_product = OrderProduct::create([
                'product_id' => $product_id,
                'order_id'   => $order_id,
                'quantity'   => 1,
            ]);
        }catch (Throwable $e){
            return false;
        }
        return $order_product;
    }
    
    public function show($order_id, $product_id, $all = false)
    {
        try{
            $order = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->with('products')->findOrFail($order_id)->products()->where('product_id', $product_id)->first();
        }catch (Throwable $e){
            return false;
        }
        return $order;
    }
    
    public function update($order_id, $product_id, $quantity = 0, $all = false)
    {
        try{
            
            $order_product = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->with('products')->findOrFail($order_id)->products()->where('product_id', $product_id)->first();
            if ($quantity) {
                $order_product->update(['quantity' => $quantity]);
            }
        }catch (Throwable $e){
            return false;
        }
        return $order_product;
    }
    
    public function destroy($order_id, $product_id, $all = false)
    {
        try{
            $order_product = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->with('products')->findOrFail($order_id)->products()->where('product_id', $product_id)->first();
            $order_product->delete();
        }catch (Throwable $e){
            return false;
        }
        return true;
    }
}
