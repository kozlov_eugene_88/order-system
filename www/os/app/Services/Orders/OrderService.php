<?php

namespace App\Services\Orders;

use App\Order;
use App\OrderAllowedStatuses;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Throwable;
use PDOException;

class OrderService
{
    public function get($all = false)
    {
        try{
            $orders = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->paginate();
        }catch (Throwable $e){
            return false;
        }
        return $orders;
    }
    
    public function create($user_id = 0)
    {
        try{
            $order = Order::create(['user_id' => $user_id ?: auth()->id(), 'status_id' => 1]);
        }catch (Throwable $e){
            return false;
        };
        return $order;
    }
    
    public function show($id, $all = false)
    {
        try{
            $order = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->findOrFail($id);
        }catch (Throwable $e){
            echo $e->getTrace();
            return false;
        }
        return $order;
    }
    
    public function update($id, $status = 0, $all = false)
    {
        try{
            $order = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->findOrFail($id);
            if ($status ? $this->canSetStatus($order->status_id, $status) : false) {
                $order->update(['status_id' => $status]);
                return $order;
            } else return false;
        }catch (Throwable $e){
            return false;
        }
    }
    
    public function destroy($id, $all = false)
    {
        try{
            $order = Order::when($all, function ($query){
                return $query->withoutGlobalScopes();
            })->findOrFail($id);
            $order->delete();
        }catch (Throwable $e){
            return false;
        }
        return true;
    }
    
    /**
     * @param $status_id
     * @param $new_status_id
     * @return bool
     */
    public function canSetStatus($status_id, $new_status_id)
    {
        $allowed_system = Cache::rememberForever('order_allowed_statuses', function (){
            return OrderAllowedStatuses::all()->groupBy('status_id')->transform(function ($group){
                return $group->transform(function ($status){
                    return $status->allowed_status_id;
                });
            })->toArray();
        });
        return in_array($new_status_id, Arr::get($allowed_system, $status_id));
    }
}
