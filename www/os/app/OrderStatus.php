<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function allowed()
    {
        return $this->hasMany('App\OrderAllowedStatuses', 'status_id');
    }
}
