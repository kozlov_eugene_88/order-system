<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAllowedStatuses extends Model
{
    public function status() {
        return $this->belongsTo('App\OrderStatuses');
    }
}
