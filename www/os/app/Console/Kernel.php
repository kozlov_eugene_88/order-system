<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\OrderCrud\OrderCreate::class,
        Commands\OrderCrud\OrderDestroy::class,
        Commands\OrderCrud\OrderGet::class,
        Commands\OrderCrud\OrderShow::class,
        Commands\OrderCrud\OrderUpdate::class,
        Commands\OrderProductCrud\OrderProductCreate::class,
        Commands\OrderProductCrud\OrderProductDestroy::class,
        Commands\OrderProductCrud\OrderProductGet::class,
        Commands\OrderProductCrud\OrderProductShow::class,
        Commands\OrderProductCrud\OrderProductUpdate::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
