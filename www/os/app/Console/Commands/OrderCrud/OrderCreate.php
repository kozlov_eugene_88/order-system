<?php

namespace App\Console\Commands\OrderCrud;

use App\Services\Orders\OrderService;
use Illuminate\Console\Command;

class OrderCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create
                            {user : Создать заказ у пользователя с данным id}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Order';
    protected $orderService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user_id = $this->argument('user');
        if ($order = $this->orderService->create($user_id)) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        } else $this->error('Что-то пошло не так!');
    }
}
