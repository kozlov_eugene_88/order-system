<?php

namespace App\Console\Commands\OrderCrud;

use App\Services\Orders\OrderService;
use Illuminate\Console\Command;

class OrderGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:get
                            {--all=1 : Показать все заказы, а не только свои}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all orders';
    protected $orderService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($orders = $this->orderService->get($this->option('all'))) {
            $headers = array_keys($orders->toArray()['data'][0]);
            $rows    = array_values($orders->toArray()['data']);
            $this->table($headers, $rows);
        } else $this->error('Что-то пошло не так!');
    }
}
