<?php

namespace App\Console\Commands\OrderCrud;

use App\Services\Orders\OrderService;
use Illuminate\Console\Command;

class OrderUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:update
                            {id : ID заказа для обновления}
                            {status=0 : ID статуса заказа, на который желаете сменить}
                            {--all=1 : Возможность удалять не только свои заказы}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status in order';
    protected $orderService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        if ($order = $this->orderService->update($id, $this->argument('status'), $this->option('all'))) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
