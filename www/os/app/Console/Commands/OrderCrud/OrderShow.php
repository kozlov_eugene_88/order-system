<?php

namespace App\Console\Commands\OrderCrud;

use App\Services\Orders\OrderService;
use Illuminate\Console\Command;
use Throwable;

class OrderShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:show
                            {id : ID заказа для просмотра}
                            {--all=1 : Возможность просмотра не только своего заказа}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show order with id {order_id}';
    protected $orderService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        if ($order = $this->orderService->show($id, $this->option('all'))) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
