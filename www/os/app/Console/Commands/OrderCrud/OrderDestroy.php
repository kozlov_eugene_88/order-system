<?php

namespace App\Console\Commands\OrderCrud;

use App\Services\Orders\OrderService;
use Illuminate\Console\Command;

class OrderDestroy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:destroy
                            {id : ID заказа для удаления}
                            {all=1 : Возможность удалять не только свои заказы}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete order';
    protected $orderService;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $response = $this->orderService->destroy($id, $this->option('all'));
        $response ? $this->info('Заказ удален') : $this->error('Что-то пошло не так!');
    }
}
