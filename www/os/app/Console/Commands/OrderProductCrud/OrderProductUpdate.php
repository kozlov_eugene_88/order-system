<?php

namespace App\Console\Commands\OrderProductCrud;

use App\Services\Orders\OrderProductService;
use Illuminate\Console\Command;

class OrderProductUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_product:update
                            {order_id : Изменить товар из заказа с данным id}
                            {product_id : Изменить товар с данным id из заказа}
                            {quantity=0 : Изменить количество товара на указанное количество}
                            {--all=1 : Использовать все заказы, а не только свои}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change product quantity in order';
    protected $ops;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderProductService $ops)
    {
        parent::__construct();
        $this->ops = $ops;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($order = $this->ops->update(
            $this->argument('order_id'), $this->argument('product_id'), $this->argument('quantity'),
            $this->option('all')
        )) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
