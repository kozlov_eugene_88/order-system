<?php

namespace App\Console\Commands\OrderProductCrud;

use App\Services\Orders\OrderProductService;
use Illuminate\Console\Command;

class OrderProductCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_product:create
                            {order_id : Добавить товар в заказ с данным id}
                            {product_id : Добавить в заказ продукт с данным id}
                            {--all=1 : Использовать все заказы, а не только свои}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a new product to an existing order. When added repeatedly, the amount will increase by 1';
    protected $ops;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderProductService $ops)
    {
        parent::__construct();
        $this->ops = $ops;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($order = $this->ops->create(
            $this->argument('order_id'), $this->argument('product_id'), $this->option('all')
        )) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
