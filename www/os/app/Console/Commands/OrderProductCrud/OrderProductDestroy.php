<?php

namespace App\Console\Commands\OrderProductCrud;

use App\Services\Orders\OrderProductService;
use Illuminate\Console\Command;

class OrderProductDestroy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_product:destroy
                            {order_id : Удалить товар в заказе с данным id}
                            {product_id : Удалить в заказе продукт с данным id}
                            {--all=1 : Использовать все заказы, а не только свои}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete product from order with {id}';
    protected $ops;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderProductService $ops)
    {
        parent::__construct();
        $this->ops = $ops;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->ops->destroy(
            $this->argument('order_id'), $this->argument('product_id'), $this->option('all')
        );
        $response ? $this->info('Продукт из заказа удален') : $this->error('Что-то пошло не так!');
    }
}
