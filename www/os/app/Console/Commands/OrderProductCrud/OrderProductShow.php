<?php

namespace App\Console\Commands\OrderProductCrud;

use App\Services\Orders\OrderProductService;
use Illuminate\Console\Command;
use Throwable;

class OrderProductShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_product:show
                            {order_id : Показать товар из заказа с данным id}
                            {product_id : Показать товар с данным id из заказа}
                            {--all=1 : Использовать все заказы, а не только свои}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show product {product_id} from order {order}';
    protected $ops;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderProductService $ops)
    {
        parent::__construct();
        $this->ops = $ops;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($order = $this->ops->show(
            $this->argument('order_id'), $this->argument('product_id'), $this->option('all')
        )) {
            $headers = array_keys($order->toArray());
            $rows    = [array_values($order->toArray())];
            $this->table($headers, $rows);
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
