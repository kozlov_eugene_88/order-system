<?php

namespace App\Console\Commands\OrderProductCrud;

use App\Services\Orders\OrderProductService;
use Illuminate\Console\Command;
use Throwable;

class OrderProductGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_product:get
                            {order_id : Показать все товары в заказе с данным id}
                            {--all=1 : Использовать все заказы, а не только свои}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all products in order {id}';
    protected $ops;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderProductService $ops)
    {
        parent::__construct();
        $this->ops = $ops;
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($response = $this->ops->get($this->argument('order_id'), $this->option('all'))) {
            try{
                $headers = array_keys($response->toArray()['data'][0]);
                $rows    = array_values($response->toArray()['data']);
                $this->table($headers, $rows);
            }catch (Throwable $e){
                $this->error('Заказ пуст');
            }
        }else {
            $this->error('Что-то пошло не так!');
        }
    }
}
