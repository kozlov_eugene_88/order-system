<?php

use Illuminate\Database\Seeder;
use App\OrderAllowedStatuses;

class OrderAllowedStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderAllowedStatuses::insert([
            ['status_id' => '1', 'allowed_status_id' => 1],
            ['status_id' => '1', 'allowed_status_id' => 2],
            ['status_id' => '1', 'allowed_status_id' => 5],
            ['status_id' => '2', 'allowed_status_id' => 2],
            ['status_id' => '2', 'allowed_status_id' => 3],
            ['status_id' => '2', 'allowed_status_id' => 5],
            ['status_id' => '3', 'allowed_status_id' => 3],
            ['status_id' => '3', 'allowed_status_id' => 4],
            ['status_id' => '3', 'allowed_status_id' => 5],
            ['status_id' => '4', 'allowed_status_id' => 4],
            ['status_id' => '5', 'allowed_status_id' => 5],
        ]);
    }
}
