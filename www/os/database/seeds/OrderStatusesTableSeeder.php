<?php

use Illuminate\Database\Seeder;
use App\OrderStatus;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::insert([
            ['name' => 'created'],
            ['name' => 'processed'],
            ['name' => 'delivering'],
            ['name' => 'completed'],
            ['name' => 'canceled'],
        ]);
    }
}
