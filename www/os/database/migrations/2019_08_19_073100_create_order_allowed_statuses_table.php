<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAllowedStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_allowed_statuses', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('allowed_status_id');
            $table->foreign('status_id')->references('id')->on('order_statuses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('allowed_status_id')->references('id')
                ->on('order_statuses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_allowed_statuses');
    }
}
