#!/bin/sh

cd /var/www/os
php /var/www/os/composer.phar install

chmod 777 -Rf /var/www/os/storage/
chmod 777 -Rf /var/www/os/bootstrap/cache/
chmod 777 -Rf /var/www/logs-os/

php /var/www/os/artisan migrate:fresh --seed
php /var/www/os/artisan passport:install
php /var/www/os/composer.phar dumpautoload

exec "$@";